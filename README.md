# Little Fluffy Location Library #

* Battery-friendly background location updates for Android, using Google Play Location where available

* v17 beta including Google Play Services (GMS) fused location provider is available now! For production code use v15

* Drop this tiny library into your Android app, configure your manifest, initialise it in your application's onCreate method... and that's it!

* Location updates will be broadcast to your app periodically, including after phone reboot. And your users won't complain about it killing their batteries.

* Perfect for widgets that require periodic updates in background, and for apps that need a reasonably current location available to them on startup. 

* Also works great for apps that do something with location periodically, such as using it to get updates from a server.

* Based on the concepts in, and some code adapted from, android-protips-location by # Reto Meier #, from his blog post A Deep Dive Into Location.

# Requires Android 2.1 and up. Works best with 2.2 and up.

* Now works with the closed-source and copyrighted Google Play Services (GMS) fused location provider, if available. If not (eg Kindle Fire, Nokia X) it falls back to using Android Open Source Project location providers.

* The library works by using Froyo's passive location listener (only possible with Android 2.2 and up, hence why it works best with it), which listens to location updates requested by other apps on your phone. The most accurate location is broadcast to your app approximately every 15 minutes. If a location update hasn't been received from another app for an hour, the library forces a location update of its own.

# Features: #

* location updates broadcast to your app approximately every 15 minutes (if a new location update was found)

* if no location update found for an hour, an update is requested
optionally, every location update is broadcast

* frequency of regular broadcasts (default 15 minutes) and forced updates (default 60 minutes) configurable

* force update feature

* get latest location feature

* choose the most accurate location from the location providers available

* abstracts away the complexity of Google Play Services versus Android Open Source

debug output optional
Written by Kenton Price, Little Fluffy Toys Ltd

Include : 
Two Project 1) Library 2) Usages of library notes that library project used google play service lib that you can found in you sdk extract directory if it not there then open the sdk manger -> at bottom extras -> Google play service download it after that you can check the location of sdk -> extras -> google 
add it for reference of about lib.